package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;


public class IntegrationTesting {


    private Activity act;
    private RepositoryActivity repositoryActivity, rep;

    private Contact con;
    private RepositoryContact repositoryContact;


    @Before
    public void setUp() throws Exception {
        repositoryActivity = new RepositoryActivityMock();
        repositoryContact = new RepositoryContactMock();
        rep = new RepositoryActivityFile(repositoryContact);
    }

    @Test
    public void testCase1()
    {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            act = new Activity("name1",
                    df.parse("03/20/2013 12:00"),
                    df.parse("03/20/2013 13:00"),
                    null,
                    "Lunch break");
            repositoryActivity.addActivity(act);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertTrue(1 == repositoryActivity.count());
    }

    @Test
    public void testCase2()
    {
        try {
            con = new Contact("name", "address1", "+4071122334455");
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }

        repositoryContact.addContact(con);
        for(Contact c : repositoryContact.getContacts())
            if (c.equals(con))
            {
                assertTrue(true);
                break;
            }
    }


    @Test
    public void testCase3() {
        for (Activity act : rep.getActivities())
            rep.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name1", start, end,
                new LinkedList<Contact>(), "description2");

        rep.addActivity(act);

        c.set(2013, 3 - 1, 20);

        List<Activity> result = rep.activitiesOnDate("name1", c.getTime());
        assertTrue(result.size() == 1);
    }

    @Test
    public void testCaseBigBang(){
        testCase1();
        testCase2();
        testCase3();
    }



    ///------------------------------------------

    @Test
    public void testCaseTopDown1(){
        testCase1();
    }

    @Test
    public void testCaseTopDown12(){
        testCase1();
        testCase2();
    }

    @Test
    public void testCaseTopDown123(){
        testCase1();
        testCase2();
        testCase3();
    }

}
